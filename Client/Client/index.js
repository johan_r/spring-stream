﻿$(window).on("load", (function () {
    if (!window.MediaSource) {
        console.error('No Media Source API available')
        return
    }

    audio = $('#audio-player').get(0)
    currentTabId = BROWSER_TAB
    initPlaylists()
    splitWindow()
    initSidebar()
    initSongControls()
    initAudioResources()
    initHeader()
    getAndAddSongsToBrowser()
    initDataTables()
    initSongRowContextMenu()
    initVisualizer()
    initKeybinds()
}))

/**
 * Layout functions
 */
function splitWindow() {
    // Splits the window into panes
    window.Split(['#header', '#content', '#footer'], {
        direction: 'vertical',
        gutterSize: 1,
        sizes: ['150px', 'calc(100vh - 302px)', '150px']
    })
}

function initSidebar() {
    $('#sidebar').sidebar({
        onHide: function () {
            displaySidebar = false
            resizeContent()
        },
        onVisible: function () {
            displaySidebar = true
            resizeContent()
        },
        closable: false,
        'dimPage': false
    }).sidebar('show')
    displaySidebar = true

    $('#sidebar-toggle').click(function () {
        $('#sidebar-toggle').toggleClass('inverted left right')
        $('#sidebar').sidebar('toggle')
    });

    $('#sidebar-visualizer').click(function (e) {
        e.preventDefault()
        changeToVisualizerTab()
    })
    
    $('#sidebar-browse').click(function (e) {
        e.preventDefault()
        changeToBrowserTab()
    })

    // Init sidebar context menu
    $.contextMenu({
        selector: ".sidebar",
        items: {
            create: {
                name: "Create new playlist",
                callback: displayCreateNewPlaylistPopup
            }
        }
    });
}

function initDataTables() {
    if (!$.fn.dataTable.isDataTable('.data-table')) {
        $('.data-table').dataTable({
            bPaginate: false,
            fixedHeader: true,
            scrollY: 'calc(100vh - 350px)',
            dom: 't',
            "columnDefs": [
                { "orderable": false, "targets": 0 },
                { "width": "30px", "targets": 0 }
            ]
        })

        $('#browser-table').DataTable().order([1, 'asc']);
        $('#playlist-table').DataTable().order([3, 'asc']);
    }
}

function resizeContent() {
    if (displaySidebar) {
        var sidebarWidth = $('#sidebar').width()
        var windowWidth = $(window).width()
        var newWidth = windowWidth - sidebarWidth + 'px'
        $('#main-content').css('width', newWidth)
    }
    else {
        $('#main-content').css('width', '100%')
    }

    if ($.fn.dataTable.isDataTable('.data-table')) {
        setTimeout(function () { $('.data-table').DataTable().columns.adjust().draw() }, 500)
    }

    setTimeout(function () {
        var canvas = $('#visualizer-canvas')
        var canvasDom = canvas.get(0)
        var canvasContainer = canvas.parent()
        canvasDom.width = canvasContainer.width()
        canvasDom.height = canvasContainer.height()
    }, 500)
};

var _resizeTimer;
$(window).resize(function () {
    if (!displaySidebar && currentTabId != VISUALIZER_TAB) {
        // Content width is already set to 100% and adjusts automatically
        return
    }

    clearTimeout(_resizeTimer);
    _resizeTimer = setTimeout(resizeContent, 100);
});

/**
 * Playlists
 */
function initPlaylists() {
    playlists = []

    // Get the users playlists
    GET({ endpoint: 'playlists/get-user-playlists' }, function (playlists) {
        var playlistCount = 0

        $.each(playlists, function (_, playlist) {
            GET({ endpoint: 'songs/playlist-songs/' + playlist.PlaylistId }, function (songs) {
                // Store playlist metadata
                var p = {
                    PlaylistId: playlist.PlaylistId,
                    Name: playlist.Name,
                    Songs: songs
                }
                this.playlists.push(p)

                if (++playlistCount == playlists.length) {
                    updatePlaylists()
                }
            })
        })
    })
}

function updatePlaylists() {
    // Add playlists to sidebar
    var playlistsHtml = ''
    $.each(playlists, function (_, playlist) {
        playlistsHtml +=
            '<a id="' + playlist.PlaylistId + '" class="item borderless subitem playlist">' +
            '<small>' + playlist.Name + '</small>' +
            '</a>'
    })

    $('.playlist').remove()
    $(playlistsHtml).insertAfter('#sidebar-playlist-header')
    
    // Bind sidebar click events
    $('.playlist').dblclick(function (e) {
        e.preventDefault()
        playPlaylist($(this).attr('id'))
    })

    $('.playlist').click(function (e) {
        e.preventDefault()
        changeToPlaylistTab($(this).text(), $(this).attr('id'))
    })
    
    // Init sidebar playlist context menu
    $.contextMenu({
        selector: ".playlist",
        items: {
            play: {
                name: "Play",
                callback: function () {
                    playPlaylist($(this).attr('id'))
                }
            },
            delete: {
                name: "Delete",
                callback: function () {
                    displayDeletelaylistPopup($(this).attr('id'), $(this).text())
                }
            }
        }
    })
}

function displayCreateNewPlaylistPopup() {
    $('#create-playlist-popup')
        .modal({
            closable: false,
            onApprove: createNewPlaylist
        })
        .modal('show')

    function createNewPlaylist() {
        var playlistName = $('#new-playlist-name').val()
        if (playlistName.length > 0 && playlistName.length < 64) {
            POST({
                endpoint: 'playlists/create',
                body: '=' + playlistName
            }, function (response) {
                if (response != GUID_EMPTY) {
                    // Add playlist
                    var playlist = {
                        PlaylistId: response,
                        Name: playlistName,
                        Songs: []
                    }
                    playlists.push(playlist)
                    updatePlaylists()

                    // Clear input field
                    $('#new-playlist-name').val('')
                }
            })
        }
    }
}

function displayDeletelaylistPopup(playlistId, playlistName) {
    var popupHeaderHtml = '<i class="remove outline icon"></i>' +
        'Are you sure you want to delete the playlist "' + playlistName + '"?'
    $('#delete-playlist-prompt').html(popupHeaderHtml)
    $('#delete-playlist-popup')
        .modal({
            closable: false,
            onApprove: deletePlaylist
        })
        .modal('show')

    function deletePlaylist() {
        POST({
            endpoint: 'playlists/delete',
            body: '=' + playlistId
        }, function (success) {
            if (success) {
                if (currentlyDisplayedPlaylistId === playlistId && currentTabId === PLAYLIST_TAB) {
                    changeToBrowserTab() // TODO Redirect to home tab when it exists
                }
                removePlaylistById(playlistId)
                updatePlaylists()
            }
        })
    }
}

function addSongToPlaylist(playlistId, songRow) {
    // Return if song is already in playlist
    if (currentlyDisplayedPlaylistId === playlistId && currentTabId === PLAYLIST_TAB) {
        // TODO: Toast?
        return
    }

    var songId = songRow.attr('id')
    var requestData = {
        PlaylistId: playlistId,
        SongId: songId
    }
    POST({
        endpoint: 'playlists/add-song-to-playlist',
        body: requestData
    }, function (songWasAdded) {
        if (!songWasAdded) {
            return
        }

        var song = {
            SongID: songId,
            Title: songRow.find('td:eq(1)').text(),
            Artists: songRow.data('artistsData'),
            DateAdded: getCurrentDateAsString()
        }
        var durationCell = songRow.find('td:last')
        song.Duration = parseFloat(durationCell.attr('songDuration'))
        song.InvariantStartPosition = parseInt(durationCell.attr('invariantStartPosition'))
        song.InvariantEndPosition = parseInt(durationCell.attr('invariantEndPosition'))
        song.OffsetBytes = song.InvariantStartPosition
        
        var playlist = getPlaylistById(playlistId)
        playlist.Songs.push(song)
    })
}

function removeSongFromPlaylist(playlistId, songId) {
    var requestData = {
        PlaylistId: playlistId,
        SongId: songId
    }
    POST({
        endpoint: 'playlists/remove-song-from-playlist',
        body: requestData
    }, function (success) {
        if (success === true) {
            var playlist = getPlaylistById(playlistId)
            playlist.Songs = playlist.Songs.filter(function (e) {
                return e.SongID != songId
            })
            $('#playlist-table').DataTable().row('#' + songId).remove().draw()
        }
    })
}

function playPlaylist(playlistId, firstSongId) {
    var playlist = getPlaylistById(playlistId)
    currentQueue.currentPlaylistId = playlistId
    startNewQueue(playlist.Songs, firstSongId, QUEUETYPE_PLAYLIST)
}

function changeToPlaylistTab(playlistName, playlistId) {
    if (currentTabId === PLAYLIST_TAB && currentlyDisplayedPlaylistId === playlistId) {
        console.log("Skipping change tab as we're already on that tab")
        return
    }

    // Change tab
    $(currentTabId).removeClass('active')
    $('#playlist-header').text(playlistName)
    currentTabId = PLAYLIST_TAB
    currentlyDisplayedPlaylistId = playlistId
    $(currentTabId).addClass('active')
    showMainContent()

    // Add songs to table
    $('#playlist-table').DataTable().clear()
    var playlist = getPlaylistById(playlistId)
    addSongsToTable(playlist.Songs, '#playlist-table')

    // Show now playing indicator
    if (currentQueue.type == QUEUETYPE_PLAYLIST && currentQueue.currentPlaylistId == playlistId) {
        $('#playlist-table > tbody > #' + currentSong.songId + ' > td').first().html('<i class="volume up icon"></i>')
    }
}

/**
 * Visualizer
 */
function changeToVisualizerTab() {
    if (currentTabId === VISUALIZER_TAB) {
        console.log("Skipping change tab as we're already on that tab")
        return
    }
    $(currentTabId).removeClass('active')
    currentTabId = VISUALIZER_TAB
    $('#header').hide()
    $('#footer').hide()
    $('#content').hide()
    $('#visualizer-content').show()
}

/**
 * Browser
 */
function changeToBrowserTab() {
    if (currentTabId === BROWSER_TAB) {
        console.log("Skipping change tab as we're already on that tab")
        return
    }
    $(currentTabId).removeClass('active')
    currentTabId = BROWSER_TAB
    $(currentTabId).addClass('active')
    showMainContent()
}

function showMainContent() {
    $('#header').show()
    $('#footer').show()
    $('#content').show()
    $('#visualizer-content').hide()

    if ($.fn.dataTable.isDataTable('.data-table')) {
        $('.data-table').DataTable().columns.adjust().draw()
    }
}

/**
* Song controls
*/
function initSongControls() {
    // Shuffle button
    $('#song-controls-button-shuffle').click(function () {
        $(this).toggleClass('disabled')
        currentQueue.shuffle = !currentQueue.shuffle
    });

    // Previous song button
    $('#song-controls-button-previous').click(function () {
        currentQueue.queueIndex--
        playNextInQueue()
    });

    // Play button
    $('#song-controls-button-play').click(function () {
        togglePlayPause()
    });

    // Loop button
    $('#song-controls-button-loop').click(function () {
        $(this).toggleClass('disabled')
        currentQueue.loop = !currentQueue.loop
    });

    // Next song button
    $('#song-controls-button-next').click(function () {
        currentQueue.queueIndex++
        playNextInQueue()
    });

    // Seeking
    $('#song-progress-bar-background').click(function (e) {
        var newProgress = e.offsetX / $(this).width()
        var newProgressSecond = getCurrentSongDurationSeconds() * newProgress
        seekToSecond(newProgressSecond)
    });

    // Volume slider
    $('#song-volume-bar-background').click(function (e) {
        var offset = e.pageY - $(this).offset().top;
        var volume = 1 - (offset / $(this).height())
        audio.volume = volume
        barHeight = volume * 80
        $('#song-volume-bar').css('height', barHeight + 'px')
    });
}

function seekToSecond(second) {
    if (audio.paused || audio.buffered.length == 0) {
        return
    }

    var offsetSeconds = getCurrentSongOffsetSeconds()

    // If seek is within buffer, simply go to position; else reload song from seek position
    if (second > (audio.buffered.start(0) + offsetSeconds)
        && second < (audio.buffered.end(0) + offsetSeconds)) {
        audio.currentTime = second - offsetSeconds
    }
    else {
        var newProgress = second / getCurrentSongDurationSeconds()
        if (newProgress > 0.999) {
            newProgress = 0.999
        }
        var newOffsetBytes = getCurrentSongDurationBytes() * newProgress
        currentSong.offsetBytes = parseInt(newOffsetBytes) + currentSong.invariantStartPosition
        reloadCurrentSong()
    }
}

function togglePlayPause() {
    if (audio.buffered.length > 0 || !audio.paused) {
        $('#song-controls-button-play').toggleClass('play pause')
        if (audio.paused) {
            audio.play()
        }
        else {
            audio.pause()
        }
    }
}

/**
 * Queue
 */
function startNewQueue(songs, firstSongId, type) {
    currentQueue.songs = songs
    currentQueue.queueIndex = 0

    // Randomize shuffle order
    shuffleOrder = []
    for (var i = 0; i < songs.length; i++) {
        shuffleOrder.push(i)
    }
    shuffleOrder = shuffleArray(shuffleOrder)
    currentQueue.shuffleOrder = shuffleOrder
    
    // If the first song is predetermined (double clicking a song row in a table, instead of double clicking the playlist name etc.), 
    // then make it the first song
    if (firstSongId) {
        var index = currentQueue.songs.map(function (s) { return s.SongID; }).indexOf(firstSongId)

        if (currentQueue.shuffle) {
            var shufIndex = currentQueue.shuffleOrder.indexOf(index)

            var temp = currentQueue.shuffleOrder[shufIndex]
            currentQueue.shuffleOrder[shufIndex] = currentQueue.shuffleOrder[0]
            currentQueue.shuffleOrder[0] = temp
        }
        else {
            currentQueue.songs = rotateArray(currentQueue.songs, index)
        }
    }

    if (type) {
        currentQueue.type = type
    }
    else {
        currentQueue.type = QUEUETYPE_BROWSER
    }
    
    playNextInQueue()
}

function playNextInQueue() {
    if (currentQueue.songs.length == 0) {
        return
    }

    console.log("currentQueue.queueIndex: " + currentQueue.queueIndex + ", currentQueue.songs.length: " + currentQueue.songs.length + ", currentQueue.loop: " + currentQueue.loop)
    // Check whether end of queue is reached
    if (currentQueue.queueIndex == currentQueue.songs.length) {
        currentQueue.queueIndex = 0
        if (!currentQueue.loop) {
            $('#song-controls-button-play').removeClass('pause')
            $('#song-controls-button-play').addClass('play')
            return
        }
    }
    else if (currentQueue.queueIndex == -1) {
        currentQueue.queueIndex = currentQueue.songs.length - 1
    }

    var songToPlay = getNextSongInQueue()
    changeSongTo(songToPlay)
}

/**
 * Audio resources
 */
function initAudioResources() {
    // Create audio resources
    mediaSource = new MediaSource()
    audio.src = URL.createObjectURL(mediaSource)
    mediaSource.addEventListener('sourceopen', function () {
        sourceBuffer = mediaSource.addSourceBuffer('audio/mpeg')
        sourceBuffer.addEventListener('updateend', function () {
            currentSong.isBufferUpdating = false
        })
    })

    // When playing the song; update ui and check whether it's time to fetch a new song chunk
    audio.addEventListener('timeupdate', function () {
        var audioRelativeTime = audio.currentTime
        var absoluteSongTime = audioRelativeTime + getCurrentSongOffsetSeconds()
        var progress = (absoluteSongTime / getCurrentSongDurationSeconds()) * 100
        $('#song-progress-bar').css('width', progress + '%')
        var mmssTime = numberToMMSSString(absoluteSongTime)
        $('#song-controls-current-time').html(mmssTime)

        if (currentSong.isBufferUpdating || currentSong.isBufferComplete || audio.buffered.length == 0) {
            return
        }

        var bufferEndTime = audio.buffered.end(0)
        var margin = bufferEndTime - audioRelativeTime
        if (margin < DEFAULT_BUFFER_MARGIN_SECONDS) {
            currentSong.isBufferUpdating = true
            currentSong.chunkCount++
            getNextSongChunk()
        }
    })

    // When completing playback of a song; play the next song
    audio.addEventListener('waiting', function () {
        var audioRelativeTime = audio.currentTime
        var absoluteSongTime = audioRelativeTime + getCurrentSongOffsetSeconds()
        var progress = (absoluteSongTime / getCurrentSongDurationSeconds())
        if (progress >= 0.999) {
            currentQueue.queueIndex++
            playNextInQueue()
        }
    }) 
}

/**
 * Visualizer
 */

function initVisualizer() {
    // Initialize audio analyser
    var ctx = new AudioContext()
    var sourceNode = ctx.createMediaElementSource(audio)
    audioAnalyser = ctx.createAnalyser()
    audioAnalyser.fftSize = 32
    audioAnalyser.smoothingTimeConstant = 0.8
    sourceNode.connect(audioAnalyser)
    sourceNode.connect(ctx.destination)
    audioAnalyserFrequencyData = new Uint8Array(audioAnalyser.frequencyBinCount)
    var fps = 60
    var updateIntervalMs = 1000 / fps
    
    visualizerColorOrder = shuffleArray(VISUALIZER_COLORS.slice())
    visualizerColorId = 0

    // Visualizer
    window.setInterval(spectrumTickHandler, updateIntervalMs)
    function spectrumTickHandler() {
        if (currentTabId == VISUALIZER_TAB && !audio.paused) {
            audioAnalyser.getByteFrequencyData(audioAnalyserFrequencyData)

            var canvas = $('#visualizer-canvas').get(0)
            var ctx = canvas.getContext("2d")
            var paddingW = 0.1
            var paddingH = 0.1
            var bandHeightMod = 0.8
            var drawableSpaceW = (1 - paddingW) * canvas.width
            var drawableSpaceH = (1 - paddingH) * canvas.height
            var fillGradient = ctx.createLinearGradient(0, 0, 0, drawableSpaceH * bandHeightMod)
            var color = visualizerColorOrder[visualizerColorId]
            var colorBright = '#' + LightenColor(color.substring(1), 20)
            fillGradient.addColorStop(0, colorBright);
            fillGradient.addColorStop(1, color);
            var bandWidth = drawableSpaceW / audioAnalyser.frequencyBinCount
            ctx.fillStyle = fillGradient;

            // Draw frequency bars
            ctx.clearRect(0, 0, canvas.width, canvas.height)
            for (var i = 0; i < audioAnalyser.frequencyBinCount; i++) {
                var bandHeight = audioAnalyserFrequencyData[i] / 255 * drawableSpaceH * bandHeightMod
                ctx.fillRect(
                    (canvas.width - drawableSpaceW) / 2 + i * bandWidth,
                    canvas.height * bandHeightMod - bandHeight,
                    bandWidth * 0.8,
                    bandHeight
                );
            }

            // Draw artist + song title
            var bottomSize = drawableSpaceH * (1 - bandHeightMod)
            var fontSize = bottomSize / 3
            ctx.textBaseline = 'top'
            ctx.fillStyle = color
            ctx.font = fontSize + 'px Song'
            ctx.fillText(
                currentSong.title.toUpperCase(),
                (canvas.width - drawableSpaceW) / 2,
                canvas.height * 0.8
            )
            ctx.font = fontSize + 'px Artist'
            ctx.fillText(
                currentSong.artist.toUpperCase(),
                (canvas.width - drawableSpaceW) / 2,
                canvas.height * 0.8 + fontSize + bottomSize / 6
            )
        }
    }
}

/**
 * Songs
 */
function getAndAddSongsToBrowser() {
    GET({
        endpoint: 'songs/all-songs'
    }, function (songs) {
        browserSongs = songs
        addSongsToTable(songs, '#browser-table') // TODO: Remove when Home-tab is start tab
    })
}

function initHeader() {
    $('#browser-search-input').keyup(function () {
        $('#browser-table').DataTable().search(this.value).draw();
    });
    $('#playlist-search-input').keyup(function () {
        $('#playlist-table').DataTable().search(this.value).draw();
    });
}

function changeSongTo(song) {
    // Set currently playing icon in table to the new song
    if (currentSong.songId != '') {
        $('tr > td:first-child').html('')
    }
    var table
    if (currentQueue.type == QUEUETYPE_PLAYLIST) {
        table = '#playlist-table'
    }
    else {
        table = '#browser-table'
    }
    $(table + ' > tbody > #' + song.SongID + ' > td').first().html('<i class="volume up icon"></i>')

    currentSong.title = song.Title
    var artistStr = ''
    for (var i = 0; i < song.Artists.length; i++) {
        artistStr += song.Artists[i].ArtistName
        if (i != song.Artists.length - 1) {
            artistStr += ', '
        }
    }
    currentSong.artist = artistStr

    currentSong.songId = song.SongID
    currentSong.isBufferUpdating = true
    currentSong.isBufferComplete = false
    currentSong.duration = song.Duration
    currentSong.invariantStartPosition = song.InvariantStartPosition
    currentSong.invariantEndPosition = song.InvariantEndPosition
    currentSong.offsetBytes = currentSong.invariantStartPosition

    $('#song-controls-current-time').html('00:00')
    var mmssDuration = numberToMMSSString(currentSong.duration)
    $('#song-controls-total-duration').html(mmssDuration)
    
    reloadCurrentSong()
}

function reloadCurrentSong() {
    audio.pause()

    // Clear current song buffer
    buffered = sourceBuffer.buffered
    if (buffered && buffered.length > 0) {
        console.log("Removing current song between " + buffered.start(0) + " and " + buffered.end(0))
        sourceBuffer.remove(buffered.start(0), buffered.end(0))
    }

    currentSong.chunkCount = 0
    getNextSongChunk()
    audio.load()
    audio.play()
    $('#song-controls-button-play').removeClass('play')
    $('#song-controls-button-play').addClass('pause')
}

var getNextSongChunkInProgress = false
function getNextSongChunk() {
    if (getNextSongChunkInProgress) {
        console.log("Skipping getNextSongChunk; awaiting response to earlier request")
        return;
    }
    getNextSongChunkInProgress = true

    var startByte = currentSong.offsetBytes + (currentSong.chunkCount * DEFAULT_BUFFER_BYTES)
    var endByte = startByte + DEFAULT_BUFFER_BYTES - 1

    if (endByte > currentSong.invariantEndPosition) {
        console.log("Will now get the LAST chunk")
        currentSong.isBufferComplete = true
        endByte = currentSong.invariantEndPosition
    }

    var isFirst = startByte == currentSong.invariantStartPosition
    GET({
        endpoint: 'songs/song/' + currentSong.songId,
        headers: {
            range: 'bytes=' + startByte + '-' + endByte,
            IsFirstChunk: isFirst
        },
        xhrFields: { responseType: 'arraybuffer' }
    }, function (data) {
        sourceBuffer.appendBuffer(data)
        getNextSongChunkInProgress = false
    })
}

/**
 * Context menus
 */
function initSongRowContextMenu() {
    $.contextMenu({
        selector: "tr",
        build: createLoadedPlaylistsItems
    })

    function createLoadedPlaylistsItems() {
        var playlistSubmenu = []
        $.each(playlists, function (_, playlist) {
            playlistSubmenu.push({
                name: playlist.Name,
                callback: function () {
                    console.log("Adding song " + $(this).attr('id') + " to playlist " + playlist.PlaylistId)
                    addSongToPlaylist(playlist.PlaylistId, $(this))
                }
            })
        })

        var items = {
            items: {
                add: {
                    name: "Add to playlist",
                    items: playlistSubmenu
                }
            }
        }

        if (currentTabId === PLAYLIST_TAB) {
            items.items.remove = {
                name: "Remove",
                callback: function () {
                    console.log("Removing song " + $(this).attr('id') + " from playlist " + currentlyDisplayedPlaylistId)
                    removeSongFromPlaylist(currentlyDisplayedPlaylistId, $(this).attr('id'))
                }
            }
        }

        return items
    }
}

/**
 * Keybinds
 */
function initKeybinds() {
    document.addEventListener("keyup", keyUpHandler, false);
    function keyUpHandler(e) {
        e.preventDefault()
        var keyCode = e.keyCode;
        switch (keyCode) {
            case 32: // Space - Play/pause
                togglePlayPause()
                break
            case 38: // Arrow up - Volume up
                var newVolume = audio.volume + 0.1
                if (newVolume > 1) {
                    newVolume = 1
                }
                audio.volume = newVolume
                barHeight = newVolume * 80
                $('#song-volume-bar').css('height', barHeight + 'px')
                break
            case 40: // Arrow down - Volume down
                var newVolume = audio.volume - 0.1
                if (newVolume < 0) {
                    newVolume = 0
                }
                audio.volume = newVolume
                barHeight = newVolume * 80
                $('#song-volume-bar').css('height', barHeight + 'px')
                break
            case 37: // Arrow left - Seek -10s
                var seekTime = getCurrentSongOffsetSeconds() + audio.currentTime - 5
                if (seekTime < 0) {
                    seekTime = 0
                }
                seekToSecond(seekTime)
                break
            case 39: // Arrow right - Seek +10s
                var seekTime = getCurrentSongOffsetSeconds() + audio.currentTime + 5
                if (seekTime < getCurrentSongDurationSeconds()) {
                    seekToSecond(seekTime)
                }
                break
            case 67: // C
                if (currentTabId == VISUALIZER_TAB) {
                    visualizerColorId++
                    if (visualizerColorId >= visualizerColorOrder.length) {
                        visualizerColorId = 0
                    }
                }
                break
        }
    }

    // Prevent arrowkeys from navigating datatables
    document.addEventListener("keydown", keyDownHandler, false);
    function keyDownHandler(e) {
        var keyCode = e.keyCode;
        switch (keyCode) {
            case 32: // Space - Play/pause
            case 38: // Arrow up - Volume up
            case 40: // Arrow down - Volume down
            case 37: // Arrow left - Seek -10s
            case 39: // Arrow right - Seek +10s
                e.preventDefault()
                break
        }
    }
}

/**
 * Helper methods
 */
function getCurrentSongDurationBytes() {
    return currentSong.invariantEndPosition - currentSong.invariantStartPosition
}

function getCurrentSongOffsetSeconds() {
    return (currentSong.offsetBytes - currentSong.invariantStartPosition)
        * MP3_320KBPS_BYTES_TO_SECONDS
}

function getCurrentSongDurationSeconds() {
    return getCurrentSongDurationBytes() * MP3_320KBPS_BYTES_TO_SECONDS
}

function addSongsToTable(songs, tableName) {
    var bodyName = tableName + ' > tbody'

    $.each(songs, function (_, song) {
        var artists = ""
        $.each(song.Artists, function (artistIndex, artist) {
            artists += artist.ArtistName
            if (artistIndex < song.Artists.length - 1) {
                artists += ", "
            }
        })
        var durationString = numberToDurationString(song.Duration)
        
        var row = '<tr id="' + song.SongID + '">' +
            '<td></td>' + 
            '<td>' + song.Title + '</td>' +
            '<td>' + artists + '</td>'

        // Playlist-table contains the extra column 'DateAdded'
        if (tableName === '#playlist-table') {
            // Remove time part from datetime object
            var dateAdded = song.DateAdded.substring(0, 10)
            row += '<td>' + dateAdded + '</td>'
        }

        row += '<td songDuration=' + song.Duration +
            ' invariantStartPosition=' + song.InvariantStartPosition +
            ' invariantEndPosition=' + song.InvariantEndPosition +
            ' style = "text-align: right;" > ' + durationString + '</td > ' + // TODO css extract
            '</tr>'

        var rowElement = $(row)
        rowElement.data('artistsData', song.Artists)
        $(tableName).DataTable().row.add(rowElement)
    })
    $(tableName).DataTable().draw()

    // Double clicking on a song in the browser changes to it
    $(bodyName + ' > tr').dblclick(function () {
        var firstSongId = $(this).attr('id')
        if (tableName == '#playlist-table') {
            playPlaylist(currentlyDisplayedPlaylistId, firstSongId)
        }
        else if (tableName == '#browser-table') {
            startNewQueue(browserSongs, firstSongId)
        }
    })
}

function getPlaylistById(playlistId) {
    return playlists.find(function (e) {
        return e.PlaylistId == playlistId
    })
}

function removePlaylistById(playlistId) {
    playlists = playlists.filter(function (e) {
        return e.PlaylistId != playlistId
    })
}

function getNextSongInQueue() {
    if (currentQueue.shuffle) {
        return currentQueue.songs[currentQueue.shuffleOrder[currentQueue.queueIndex]]
    }
    else {
        return currentQueue.songs[currentQueue.queueIndex]
    }
}

/**
 * Variables
 */
var audio
var mediaSource
var sourceBuffer
var audioAnalyser
var audioAnalyserFrequencyData
var displaySidebar
var currentSong = {
    title: '',
    artist: '',
    songId: '',
    invariantStartPosition: 0,
    invariantEndPosition: 0,
    offsetBytes: 0,
    chunkCount: 0,
    isBufferUpdating: false,
    isBufferComplete: false // Means audio buffer contains the end of the song
}
var currentTabId = ''
var currentlyDisplayedPlaylistId = '' // Currently viewed in the content browser; NOT necessarily currently playing. Used to save performance to prevent reloading what's already displaying.
var playlists = [] // { playlistId: "", name: "", songs: []}
var browserSongs = [] 
var currentQueue = {
    songs: [],
    shuffleOrder: [],
    queueIndex: 0,
    shuffle: true,
    loop: true,
    type: 0, // QUEUETYPE
    currentPlaylistId: '' // Set if type is QUEUETYPE_PLAYLIST
}
var visualizerColorId = 0
var visualizerColorOrder = []

/**
 * Constants
 */
const MP3_320KBPS_SECONDS_TO_BYTES = 40000
const MP3_320KBPS_BYTES_TO_SECONDS = 0.000025
const DEFAULT_BUFFER_MARGIN_SECONDS = 15
const DEFAULT_BUFFER_SECONDS = 30
const DEFAULT_BUFFER_BYTES = DEFAULT_BUFFER_SECONDS * MP3_320KBPS_SECONDS_TO_BYTES
const PLAYLIST_TAB = '.playlist-tab'
const BROWSER_TAB = '.browser-tab'
const VISUALIZER_TAB = '.visualizer-tab'
const QUEUETYPE_BROWSER = 0
const QUEUETYPE_PLAYLIST = 1
const GUID_EMPTY = '00000000-0000-0000-0000-000000000000'

const VISUALIZER_COLORS = [
    '#FF8598', // PINK
    '#F2385A', // PINK (Dark)
    '#7D8A2E', // OLIVE (Dark)
    '#A9CF54', // OLIVE (Light)
    '#588F27', // GREEN (Dark)
    '#13BF7F', // GREEN (Hints of turquoise)
    '#C9D787', // GREEN/YELLOW (Very light)
    '#F28B30', // ORANGE (Dark)
    '#F5A503', // ORANGE
    '#FF6138', // RED/ORANGE
    '#DC3522', // RED
    '#703030', // RED (Wine)
    '#644D52', // BROWN/PURPLE
    '#2F343B', // BLACK (Coal)
    '#01A2A6', // TURQUOISE
    '#3498DB', // BLUE (Light)
    '#2980B9', // BLUE (Dark)
    '#3F5765', // BLUE/GRAY (Light)
    '#2B3A42', // BLUE/GRAY (Dark)
]