﻿const API = 'http://localhost:50482/api/v1/'

function GET(params, callback) {
    // Define default error action which is overridable
    var error;
    if (params.error) {
        error = params.error
    }
    else {
        error = function (xhr) {
            if (xhr.status == 401) {
                window.location.replace('../login.html')
            }
        }
    }

    $.ajax({
        type: 'GET',
        url: API + params.endpoint,
        crossOrigin: true,
        headers: params.headers,
        xhrFields: params.xhrFields,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", 'Bearer ' + sessionStorage.AuthToken)
        },
        success: function (response) {
            if (callback) {
                callback(response)
            }
        },
        error: error
    })
}

function POST(params, callback) {
    // Define default error action which is overridable
    var error;
    if (params.error) {
        error = params.error
    }
    else {
        error = function (xhr) {
            if (xhr.status == 401) {
                window.location.replace('../login.html')
            }
        }
    }

    $.ajax({
        type: 'POST',
        url: API + params.endpoint,
        crossOrigin: true,
        headers: params.headers,
        xhrFields: params.xhrFields,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", 'Bearer ' + sessionStorage.AuthToken)
        },
        data: params.body,
        success: function (response) {
            if (callback) {
                callback(response)
            }
        },
        error: error
    })
}