﻿$(window).on("load", (function () {
    $('.ui.form').form({
        fields: {
            username: {
                identifier: 'username',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter your username'
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter your password'
                    }
                ]
            }
        },
        onSuccess: login
    })

    $('.ui.dimmer').dimmer('set disabled')
    $('.ui.dimmer').hide() // Required to prevent a bug which causes a small square to remain dimmed
}))

function login(event, fields) {
    event.preventDefault()

    var requestData = {
        Username: fields.username,
        Password: fields.password
    }

    var onError =  function () {
        $('.ui.form').form('add errors', { general: 'Login failed - Can\'t connect to login server' })
    }

    POST({
        endpoint: 'users/login',
        body: requestData,
        error: onError
    }, function (response) {
        if (response.Success) {
            sessionStorage.AuthToken = response.AuthToken
            window.location.href = 'index.html'
        }
        else {
            onError()
        }
    })
}