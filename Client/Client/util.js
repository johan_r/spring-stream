﻿function numberToDurationString(number) {
    var intNum = parseInt(number, 10)
    var minutes = Math.floor(intNum / 60);
    var seconds = intNum - (minutes * 60);
    if (seconds < 10) { seconds = "0" + seconds; }
    return minutes + ':' + seconds;
}

function numberToMMSSString(number) {
    var intNum = parseInt(number, 10)
    var minutes = Math.floor(intNum / 60);
    var seconds = intNum - (minutes * 60);
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return minutes + ':' + seconds;
}

function getCurrentDateAsString() {
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    if (day < 10) {
        day = '0' + day
    }
    if (month < 10) {
        month = '0' + month
    }
    var dateString = year + '-' + month + '-' + day
    return dateString
}

function shuffleArray(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

function rotateArray(arr, n) {
    return arr.slice(n).concat(arr.slice(0, n));
}