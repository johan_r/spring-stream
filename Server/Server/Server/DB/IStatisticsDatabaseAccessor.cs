﻿using System;

namespace Server.DB
{
    public interface IStatisticsDatabaseAccessor
    {
        void AddSongPlay(Guid songId, int userId);
    }
}
