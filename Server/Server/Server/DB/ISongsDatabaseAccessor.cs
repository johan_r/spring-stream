﻿using Server.Model.Database;
using System;
using System.Collections.Generic;

namespace Server.DB
{
    public interface ISongsDatabaseAccessor
    {
        string GetSongPath(Guid songId);
        List<Song> GetAllSongs();
        List<Song> GetPlaylistSongs(Guid playlistId);
        void AddSongs(List<Song> songs);
        void AddArtists(List<Artist> artists);
        void AddArtistSongs(List<ArtistSong> artistSongs);
        void Clear();
        bool UpdateSongPath(byte[] songMd5, string path);
    }
}
