﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using FastMember;
using Server.Model.Database;

namespace Server.DB
{
    public class SongsDatabaseAccessor : ISongsDatabaseAccessor
    {
        public string GetSongPath(Guid songId)
        {
            string sql = @"
                SELECT Path 
                FROM Song 
                WHERE SongID = @SongID";
            
            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@SongID", songId);
                conn.Open();
                var path = (string)command.ExecuteScalar();
                conn.Close();
                return path;
            }
        }

        public void AddArtists(List<Artist> artists)
        {
            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var bulkCopy = new SqlBulkCopy(conn))
            using (var reader = ObjectReader.Create(artists))
            {
                bulkCopy.ColumnMappings.Add("ArtistID", "ArtistID");
                bulkCopy.ColumnMappings.Add("ArtistName", "ArtistName");
                bulkCopy.DestinationTableName = "Artist";
                conn.Open();
                bulkCopy.WriteToServer(reader);
                conn.Close();
            }
        }

        public void AddSongs(List<Song> songs)
        {
            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var bulkCopy = new SqlBulkCopy(conn))
            using (var reader = ObjectReader.Create(songs))
            {
                bulkCopy.ColumnMappings.Add("SongID", "SongID");
                bulkCopy.ColumnMappings.Add("Path", "Path");
                bulkCopy.ColumnMappings.Add("Title", "Title");
                bulkCopy.ColumnMappings.Add("DateAdded", "DateAdded");
                bulkCopy.ColumnMappings.Add("Duration", "Duration");
                bulkCopy.ColumnMappings.Add("InvariantStartPosition", "InvariantStartPosition");
                bulkCopy.ColumnMappings.Add("InvariantEndPosition", "InvariantEndPosition");
                bulkCopy.ColumnMappings.Add("Md5Checksum", "Md5Checksum");
                bulkCopy.DestinationTableName = "Song";
                conn.Open();
                bulkCopy.WriteToServer(reader);
                conn.Close();
            }
        }
        
        public void AddArtistSongs(List<ArtistSong> artistSongs)
        {
            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var bulkCopy = new SqlBulkCopy(conn))
            using (var reader = ObjectReader.Create(artistSongs))
            {
                bulkCopy.ColumnMappings.Add("ArtistID", "ArtistID");
                bulkCopy.ColumnMappings.Add("SongID", "SongID");
                bulkCopy.DestinationTableName = "ArtistSong";
                conn.Open();
                bulkCopy.WriteToServer(reader);
                conn.Close();
            }
        }

        public void Clear()
        {
            string[] sql = new[]{
                "DELETE FROM PlaylistSong",
                "DELETE FROM ArtistSong",
                "DELETE FROM Song",
                "DELETE FROM Playlist",
                "DELETE FROM Artist"
            };

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(null, conn))
            {
                conn.Open();
                foreach (var query in sql)
                {
                    command.CommandText = query;
                    command.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        public List<Song> GetAllSongs()
        {
            string sql = @"
                SELECT Song.Title, Artist.ArtistName, Song.SongID, Artist.ArtistID, Song.Duration, Song.InvariantStartPosition, Song.InvariantEndPosition, Song.Md5Checksum, Song.Path 
                FROM Artist, Song, ArtistSong
                WHERE Artist.ArtistID = ArtistSong.ArtistID
                    AND Song.SongID = ArtistSong.SongID";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                conn.Open();
                var songs = GetSongs(command);
                conn.Close();
                return songs;
            }
        }

        public List<Song> GetPlaylistSongs(Guid playlistId)
        {
            string sql = @"
                SELECT Song.Title, Artist.ArtistName, Song.SongID, Artist.ArtistID, Song.Duration, Song.InvariantStartPosition, Song.InvariantEndPosition, Song.Md5Checksum, Song.Path, PlaylistSong.DateAdded 
                FROM Artist, Song, ArtistSong, Playlist, PlaylistSong
                WHERE Artist.ArtistID = ArtistSong.ArtistID
                    AND Song.SongID = ArtistSong.SongID
                    AND PlaylistSong.DateRemoved IS NULL 
	                AND Song.SongID = PlaylistSong.SongID
	                AND Playlist.PlaylistID = PlaylistSong.PlaylistID
                    AND Playlist.DateDeleted IS NULL
	                AND Playlist.PlaylistID = @PlaylistID";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@PlaylistID", playlistId);
                conn.Open();
                var songs = GetSongs(command);
                conn.Close();
                return songs;
            }
        }

        private List<Song> GetSongs(SqlCommand command)
        {
            // Songs can have several rows returned if there is more than one artist entry
            var songs = new Dictionary<Guid, Song>(); // Guid here is SongID
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var songId = (Guid)reader[2];
                    songs.TryGetValue(songId, out Song existingSong);
                    if (existingSong == null)
                    {
                        var artistName = (string)reader[1];
                        var title = (string)reader[0];
                        var artistId = (Guid)reader[3];
                        var songDuration = (double)reader[4];
                        var invariantStartPosition = (long)reader[5];
                        var invariantEndPosition = (long)reader[6];
                        var md5Chesksum = (byte[])reader[7];
                        var path = (string)reader[8];

                        var artists = new List<Artist>();
                        var artist = new Artist
                        {
                            ArtistName = artistName,
                            ArtistID = artistId
                        };
                        artists.Add(artist);
                        var newSong = new Song()
                        {
                            SongID = songId,
                            Title = title,
                            Artists = artists,
                            Duration = songDuration,
                            InvariantStartPosition = invariantStartPosition,
                            InvariantEndPosition = invariantEndPosition,
                            Md5Checksum = md5Chesksum,
                            Path = path
                        };

                        // If song is in a playlist an extra attribute (PlaylistSong.DateAdded) is returned
                        if (reader.VisibleFieldCount > 9)
                        {
                            newSong.DateAdded = (DateTime)reader[9];
                        }

                        songs[songId] = newSong;
                    }
                    else
                    {
                        var artistName = (string)reader[1];
                        var artistId = (Guid)reader[3];
                        var artist = new Artist
                        {
                            ArtistName = artistName,
                            ArtistID = artistId
                        };
                        existingSong.Artists.Add(artist);
                    }
                }
            }

            var result = songs.Values.ToList();
            return result;
        }

        public bool UpdateSongPath(byte[] songMd5, string path)
        {
            string sql = @"
                UPDATE Song
                SET Path = @Path
                WHERE Md5Checksum = @Md5Checksum";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@Path", path);
                command.Parameters.AddWithValue("@Md5Checksum", songMd5);
                conn.Open();
                var rowsChanged = command.ExecuteNonQuery();
                conn.Close();
                return rowsChanged != 0;
            }
        }
    }
}