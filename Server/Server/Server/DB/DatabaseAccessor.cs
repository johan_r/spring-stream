﻿namespace Server.DB
{
    public class DatabaseAccessor
    {
        public static IUsersDatabaseAccessor Users { get; }
        public static ISongsDatabaseAccessor Songs { get; }
        public static IPlaylistsDatabaseAccessor Playlists { get; }
        public static IStatisticsDatabaseAccessor Statistics { get; }

        static DatabaseAccessor() {
            Users = new UsersDatabaseAccessor();
            Songs = new SongsDatabaseAccessor();
            Playlists = new PlaylistsDatabaseAccessor();
            Statistics = new StatisticsDatabaseAccessor();
        }
    }
}