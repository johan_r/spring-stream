﻿using Server.Model.Database;
using System;
using System.Collections.Generic;

namespace Server.DB
{
    public interface IPlaylistsDatabaseAccessor
    {
        List<Playlist> GetUserPlaylists(int userId);
        void CreatePlaylist(Playlist playlist);
        bool AddSongToPlaylist(Guid playlistId, Guid songId);
        void RemoveSongFromPlaylist(Guid playlistId, Guid songId);
        void DeletePlaylist(Guid playlistId);
    }
}
