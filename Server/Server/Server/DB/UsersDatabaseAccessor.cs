﻿using Server.Model.Database;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Server.DB
{
    public class UsersDatabaseAccessor : IUsersDatabaseAccessor
    {
        public bool UsernameExists(string username)
        {
            string sql = "SELECT 1 FROM User WHERE Username = @Username";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@Username", username);
                conn.Open();
                object result = command.ExecuteScalar();
                conn.Close();
                return result != null;
            }
        }

        public bool EmailExists(string email)
        {
            string sql = "SELECT 1 FROM User WHERE Email = @Email";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@Email", email);
                conn.Open();
                object result = command.ExecuteScalar();
                conn.Close();
                return result != null;
            }
        }

        public bool AddUser(string username, string email, byte[] password, byte[] passwordSalt, int passwordIterations, DateTime registerDate)
        {
            string sql = "INSERT INTO User (Username, Email, Password, PasswordSalt, PasswordIterations, RegisterDate) " +
                "VALUES (@Username, @Email, @Password, @PasswordSalt, @PasswordIterations, @RegisterDate)";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@Username", username);
                command.Parameters.AddWithValue("@Email", email);
                command.Parameters.AddWithValue("@Password", password);
                command.Parameters.AddWithValue("@PasswordSalt", passwordSalt);
                command.Parameters.AddWithValue("@PasswordIterations", passwordIterations);
                command.Parameters.AddWithValue("@RegisterDate", registerDate);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
                return true;
            }
        }

        public User GetUserByUserID(int userId)
        {
            string sql = "SELECT UserID, Username, Email, Password, PasswordSalt, PasswordIterations, RegisterDate " +
                "FROM [User] WHERE UserID = @UserID";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@UserID", userId);
                conn.Open();
                User user = GetUser(command);
                conn.Close();
                return user;
            }
        }

        public User GetUserByUsername(string username)
        {
            string sql = "SELECT UserID, Username, Email, Password, PasswordSalt, PasswordIterations, RegisterDate " +
                "FROM [User] WHERE Username = @Username";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@Username", username);
                conn.Open();
                User user = GetUser(command);
                conn.Close();
                return user;
            }
        }

        private User GetUser(SqlCommand command)
        {
            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    User user = new User
                    {
                        UserID = (int)reader[0],
                        Username = (string)reader[1],
                        Email = (string)reader[2],
                        Password = (byte[])reader[3],
                        PasswordSalt = (byte[])reader[4],
                        PasswordIterations = (int)reader[5],
                        RegisterDate = (DateTime)reader[6]
                    };
                    return user;
                }
            }
            return null;
        }

        public int? GetUserIDByUsername(string username)
        {
            string sql = "SELECT UserID FROM User WHERE Username = @Username";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@Username", username);
                conn.Open();
                int? userId = (int?)command.ExecuteScalar();
                conn.Close();
                return userId;
            }
        }

        public string[] GetUserRoles(int userId)
        {
            string sql = @"SELECT Role.Role
                           FROM [User], UserRole, Role
                           WHERE [User].UserID = @UserID
                               AND [User].UserID = UserRole.UserID
                               AND UserRole.RoleID = Role.RoleID";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                var roles = new List<string>();
                command.Parameters.AddWithValue("@UserID", userId);
                conn.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        roles.Add((string)reader[0]);
                    }
                }
                conn.Close();
                return roles.ToArray();
            }
        }
    }
}