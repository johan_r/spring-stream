﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Server.Model.Database;

namespace Server.DB
{
    public class PlaylistsDatabaseAccessor : IPlaylistsDatabaseAccessor
    {
        public List<Playlist> GetUserPlaylists(int userId)
        {
            string sql = @"
                SELECT PlaylistID, Name 
                FROM Playlist
                WHERE DateDeleted IS NULL  
                    AND UserID = @UserID";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@UserID", userId);

                var playlists = new List<Playlist>();
                conn.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var playlistId = (Guid)reader[0];
                        var playlistName = (string)reader[1];
                        var playlist = new Playlist
                        {
                            PlaylistId = playlistId,
                            Name = playlistName
                        };
                        playlists.Add(playlist);
                    }
                }
                conn.Close();
                return playlists;
            }
        }

        public void CreatePlaylist(Playlist playlist)
        {
            string sql = @"
                INSERT INTO Playlist (PlaylistID, UserID, DateCreated, Name) 
                VALUES (@PlaylistID, @UserID, @DateCreated, @Name)";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@PlaylistID", playlist.PlaylistId);
                command.Parameters.AddWithValue("@UserID", playlist.UserId);
                command.Parameters.AddWithValue("@DateCreated", DateTime.Now.Date);
                command.Parameters.AddWithValue("@Name", playlist.Name);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public bool AddSongToPlaylist(Guid playlistId, Guid songId)
        {
            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            {
                // First test whether the song exists/has existed in the playlist before,
                // if so simply remove the DateRemoved if needed
                PlaylistSongStatus status = TestPlaylistSongExists(conn, playlistId, songId);
                switch (status)
                {
                    case PlaylistSongStatus.IN_PLAYLIST:
                        // The song is already in the playlist; do nothing
                        return false;
                    case PlaylistSongStatus.IN_PLAYLIST_REMOVED:
                        // The song has been in the playlist but is now removed; 
                        // Set DateRemoved to NULL to bring it back
                        ClearPlaylistSongRemovedDate(conn, playlistId, songId);
                        return true;
                    case PlaylistSongStatus.NOT_IN_PLAYLIST:
                        // Insert new entry
                        InsertPlaylistSong(conn, playlistId, songId);
                        return true;
                    default:
                        return false;
                }
            }
        }

        public void RemoveSongFromPlaylist(Guid playlistId, Guid songId)
        {
            string sql = @"
                UPDATE PlaylistSong 
                SET DateRemoved = @DateRemoved
                WHERE PlaylistID = @PlaylistID
                    AND SongID = @SongID";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@DateRemoved", DateTime.Now.Date);
                command.Parameters.AddWithValue("@PlaylistID", playlistId);
                command.Parameters.AddWithValue("@SongID", songId);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void DeletePlaylist(Guid playlistId)
        {
            string sql = @"
                UPDATE Playlist 
                SET DateDeleted = @DateDeleted
                WHERE PlaylistID = @PlaylistID";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@DateDeleted", DateTime.Now.Date);
                command.Parameters.AddWithValue("@PlaylistID", playlistId);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        private PlaylistSongStatus TestPlaylistSongExists(SqlConnection conn, Guid playlistId, Guid songId)
        {
            string sql = @"
                SELECT DateRemoved 
                FROM PlaylistSong 
                WHERE PlaylistID = @PlaylistID
                    AND SongID = @SongID";

            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@PlaylistID", playlistId);
                command.Parameters.AddWithValue("@SongID", songId);
                conn.Open();
                var result = command.ExecuteScalar();
                conn.Close();
                
                if (result != null)
                {
                    if (result == DBNull.Value)
                    {
                        return PlaylistSongStatus.IN_PLAYLIST;
                    }
                    return PlaylistSongStatus.IN_PLAYLIST_REMOVED;
                }
                return PlaylistSongStatus.NOT_IN_PLAYLIST;
            }
        }

        private void ClearPlaylistSongRemovedDate(SqlConnection conn, Guid playlistId, Guid songId)
        {
            string sql = @"
                UPDATE PlaylistSong  
                SET DateRemoved = NULL
                WHERE PlaylistID = @PlaylistID
                    AND SongID = @SongID";

            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@PlaylistID", playlistId);
                command.Parameters.AddWithValue("@SongID", songId);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void InsertPlaylistSong(SqlConnection conn, Guid playlistId, Guid songId)
        {
            string sql = @"
                INSERT INTO PlaylistSong (PlaylistID, SongID, DateAdded) 
                VALUES (@PlaylistID, @SongID, @DateAdded)";

            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@PlaylistID", playlistId);
                command.Parameters.AddWithValue("@SongID", songId);
                command.Parameters.AddWithValue("@DateAdded", DateTime.Now.Date);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        private enum PlaylistSongStatus
        {
            NOT_IN_PLAYLIST,
            IN_PLAYLIST,
            IN_PLAYLIST_REMOVED
        }
    }
}