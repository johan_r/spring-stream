﻿using Server.Model.Database;
using System;

namespace Server.DB
{
    public interface IUsersDatabaseAccessor
    {
        bool UsernameExists(string username);
        bool EmailExists(string email);
        bool AddUser(string username, string email, byte[] password, byte[] passwordSalt, int passwordIterations, DateTime registerDate);
        User GetUserByUserID(int userId);
        User GetUserByUsername(string username);
        int? GetUserIDByUsername(string username);
        string[] GetUserRoles(int userId);
    }
}
