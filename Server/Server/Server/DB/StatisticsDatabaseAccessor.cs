﻿using System;
using System.Data.SqlClient;

namespace Server.DB
{
    public class StatisticsDatabaseAccessor : IStatisticsDatabaseAccessor
    {
        public void AddSongPlay(Guid songId, int userId)
        {
            string sql = "INSERT INTO SongPlay (SongID, UserID, DatePlayed) " +
                "VALUES (@SongID, @UserID, @DatePlayed)";

            using (SqlConnection conn = ConnectionManager.GetDefaultConnection())
            using (var command = new SqlCommand(sql, conn))
            {
                command.Parameters.AddWithValue("@SongID", songId);
                command.Parameters.AddWithValue("@UserID", userId);
                command.Parameters.AddWithValue("@DatePlayed", DateTime.Now);
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}