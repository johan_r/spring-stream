﻿namespace Server.Model
{
    public class Range
    {
        public int Lower { get; set; }
        public int Upper { get; set; }
    }
}