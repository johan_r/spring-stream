﻿using System;
using System.Collections.Generic;

namespace Server.Model.Database
{
    public class Song
    {
        public Guid SongID { get; set; }
        public string Path { get; set; }
        public string Title { get; set; }
        public DateTime DateAdded { get; set; }
        public List<Artist> Artists { get; set; }
        public double Duration { get; set; }
        public long InvariantStartPosition { get; set; }
        public long InvariantEndPosition { get; set; }
        public byte[] Md5Checksum { get; set; }
    }
}