﻿using System;

namespace Server.Model.Database
{
    public class ArtistSong
    {
        public Guid ArtistID { get; set; }
        public Guid SongID { get; set; }
    }
}