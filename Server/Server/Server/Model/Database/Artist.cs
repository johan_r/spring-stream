﻿using System;

namespace Server.Model.Database
{
    public class Artist
    {
        public Guid ArtistID { get; set; }
        public string ArtistName { get; set; }
    }
}