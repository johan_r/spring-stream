﻿using System;

namespace Server.Model.Database
{
    public class Playlist
    {
        public Guid PlaylistId { get; set; }
        public int UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Name { get; set; }
    }
}