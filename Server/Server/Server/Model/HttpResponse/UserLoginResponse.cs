﻿namespace Server.Model.HttpResponse
{
    public class UserLoginResponse
    {
        public bool Success { get; set; }
        public string AuthToken { get; set; }
    }
}