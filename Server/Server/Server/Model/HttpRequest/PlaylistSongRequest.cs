﻿using System;

namespace Server.Model.HttpRequest
{
    public class PlaylistSongRequest
    {
        public Guid PlaylistId { get; set; }
        public Guid SongId { get; set; }
    }
}