﻿namespace Server.Model
{
    public class PasswordHash
    {
        public byte[] Password { get; set; }
        public byte[] Salt { get; set; }
        public int Iterations { get; set; }
    }
}