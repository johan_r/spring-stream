﻿using NAudio.Wave;
using Server.Model;
using Server.Model.Database;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace Server
{
    public class IO
    {
        public static byte[] GetSongChunk(string path, long from, long to)
        {
            using (BinaryReader reader = new BinaryReader(File.OpenRead(path)))
            {
                if (to >= reader.BaseStream.Length)
                {
                    to = reader.BaseStream.Length - 1;
                }

                var length = (int)(to - from + 1);
                if (length <= 0)
                {
                    throw new ArgumentException("Range is out of bounds.");
                }
                if (length > 1200000) // TODO move (320 kbps, 30 seconds)
                {
                    throw new ArgumentException("Range is too large.");
                }

                var buffer = new byte[length];
                reader.BaseStream.Seek(from, SeekOrigin.Begin);
                reader.Read(buffer, 0, length);
                return buffer;
            }
        }

        public static byte[] GetSong(string path)
        {
            using (BinaryReader reader = new BinaryReader(File.OpenRead(path)))
            {
                var length = (int)reader.BaseStream.Length;
                var buffer = new byte[length];
                reader.Read(buffer, 0, length);
                return buffer;
            }
        }

        public static List<SongMetadata> GetSongsMetadata()
        {
            string[] songPaths = Directory.GetFiles(@"G:\Music", @"*.mp3", SearchOption.AllDirectories);
            var metaList = new List<SongMetadata>();

            using (var md5 = MD5.Create())
            {
                foreach (var path in songPaths)
                {
                    // We use NAudio to determine song duration, because taglib is prone to give incorrect duration data
                    double duration;
                    using (var nAudioReader = new Mp3FileReader(path))
                    {
                        duration = nAudioReader.TotalTime.TotalSeconds;
                    }

                    // Calculate MD5 checksum of song file
                    byte[] checksum;
                    using (var file = File.OpenRead(path))
                    {
                        checksum = md5.ComputeHash(file);
                    }

                    // Get the rest of the song metadata using TagLib
                    using (var file = TagLib.File.Create(path))
                    {
                        var tag = file.GetTag(TagLib.TagTypes.Id3v2);
                        
                        // TagLib is hardcoded to split artists by '/', which forces us to manually split artists by the standard ', '
                        string artistNamesStr = tag.Performers.First();
                        artistNamesStr = artistNamesStr.Replace(@"%\", @"/");
                        string[] artistNames = artistNamesStr.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                        
                        // If essential data is missing; skip file
                        if (string.IsNullOrEmpty(tag.Title))
                        {
                            LogFileSkipped(path, "Title missing");
                            continue;
                        }
                        if (artistNames.Length == 0)
                        {
                            LogFileSkipped(path, "Artist(s) missing");
                            continue;
                        }

                        var bitrate = new Decimal((file.InvariantEndPosition - file.InvariantStartPosition) * 0.008 / duration);
                        int bitrateRounded = (int)Math.Round(bitrate, 0);
                        if (bitrateRounded != 320)
                        {
                            LogFileSkipped(path, "Invalid bitrate(" + bitrateRounded + ") " + duration);
                            continue;
                        }

                        var artists = new List<Artist>();
                        foreach (var artistName in artistNames)
                        {
                            if (artists.Any(a => a.ArtistName == artistName))
                            {
                                continue;
                            }

                            var artist = new Artist
                            {
                                ArtistName = artistName
                            };
                            artists.Add(artist);
                        }

                        var meta = new SongMetadata
                        {
                            Path = path,
                            Title = tag.Title,
                            Artists = artists,
                            Duration = duration,
                            InvariantStartPosition = file.InvariantStartPosition,
                            InvariantEndPosition = file.InvariantEndPosition,
                            Md5Checksum = checksum
                        };
                        metaList.Add(meta);
                    }
                }
            }
            return metaList;
        }

        private static void LogFileSkipped(string path, string reason)
        {
            // TODO: Log
            Debug.WriteLine("File skipped: " + path + ". Reason: " + reason + ".");
        }
    }
}