﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace Server.Security
{
    public class JwtAuthenticationFilter : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple => throw new NotImplementedException();

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            HttpRequestMessage request = context.Request;
            AuthenticationHeaderValue authorization = request.Headers.Authorization;
            
            if (authorization == null || authorization.Scheme != "Bearer")
            {
                return;
            }

            if (string.IsNullOrEmpty(authorization.Parameter))
            {
                context.ErrorResult = new UnauthorizedResult(Enumerable.Empty<AuthenticationHeaderValue>(), context.Request);
                return;
            }
            
            string token = authorization.Parameter;
            if (Jwt.ValidateJwtToken(token))
            {
                // Decode token payload to aquire user data for the principal
                var decodedToken = Jwt.DecodeJwtToken(token);
                string username = decodedToken.Payload.Claims.First(c => c.Type == "username").Value;
                string[] userRoles = decodedToken.Payload.Claims.Where(c => c.Type == "userRoles").Select(c => c.Value).ToArray();

                var principal = new GenericPrincipal(new GenericIdentity(username), userRoles);
                context.Principal = principal;
            }
            else
            {
                context.ErrorResult = new UnauthorizedResult(Enumerable.Empty<AuthenticationHeaderValue>(), context.Request);
            }
        }

        public async Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            // TODO
            return;
        }
    }
}