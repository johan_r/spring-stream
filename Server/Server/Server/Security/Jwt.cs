﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;

namespace Server.Security
{
    public class Jwt
    {
        private const string Secret = "cec55da05d431812ec6a29c578d236ebfa69aec59ed14461a91ee029300397916f73ab94f7489b24b1edd6fb940cede3200eba90f32813c7ba83c4b78523e49fe7039dae85922d88e5b34624d92c9d0b544596b507ab4f704510c1a53c7bab684f0946fb854844d189b83f81047730e6d16a08ed7cb158625d0cb3a46ca6cad4";
        private static JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
        private static SymmetricSecurityKey signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
        
        public static string GenerateJwtToken(int userId, string username, string[] userRoles)
        {
            // Header
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha256Digest);
            var header = new JwtHeader(signingCredentials);

            // Payload
            var issueTime = DateTimeOffset.Now.ToUnixTimeSeconds();
            var expirationTime = DateTimeOffset.Now.AddDays(7).ToUnixTimeSeconds();
            var payload = new JwtPayload
            {
                { "iat", issueTime },
                { "exp",  expirationTime },
                { "userId", userId },
                { "username", username },
                { "userRoles", userRoles }
            };
            
            var token = new JwtSecurityToken(header, payload);
            return tokenHandler.WriteToken(token);
        }

        public static bool ValidateJwtToken(string token)
        {
            var validationParameters = new TokenValidationParameters
            {
                RequireExpirationTime = true,
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey
            };

            SecurityToken validatedToken;
            try
            {
                tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
            }
            catch (Exception)
            {
                return false;
            }
            return validatedToken != null;
        }

        public static JwtSecurityToken DecodeJwtToken(string token)
        {
            return tokenHandler.ReadJwtToken(token);
        }

        public static int GetUserIdFromToken(string token)
        {
            var decodedToken = DecodeJwtToken(token);
            return int.Parse(decodedToken.Payload.Claims.First(c => c.Type == "userId").Value);
        }
    }
}