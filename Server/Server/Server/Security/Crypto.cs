﻿using Server.Model;
using System.Linq;
using System.Security.Cryptography;

namespace Server.Security
{
    public class Crypto
    {
        private const int HashIterations = 100000;

        public static PasswordHash GeneratePasswordHash(string password)
        {
            var salt = new byte[16];
            using (var cryptoProvider = new RNGCryptoServiceProvider())
            {
                cryptoProvider.GetBytes(salt);
            }
            
            var rfc2898 = new Rfc2898DeriveBytes(password, salt, HashIterations);
            var passwordHashBytes = rfc2898.GetBytes(20);
            var passwordHash = new PasswordHash
            {
                Password = passwordHashBytes,
                Salt = salt,
                Iterations = HashIterations
            };
            return passwordHash;
        }

        public static bool AuthenticateUser(string password, byte[] referenceHash, byte[] referenceSalt, int referenceIterations)
        {
            var rfc2898 = new Rfc2898DeriveBytes(password, referenceSalt, referenceIterations);
            var hash = rfc2898.GetBytes(20);
            return hash.SequenceEqual(referenceHash);
        }
    }
}