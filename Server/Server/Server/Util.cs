﻿using Server.Model;
using System;
using System.Text.RegularExpressions;

namespace Server
{
    public class Util
    {
        public static bool StringEqualsIgnoreCase(string s1, string s2)
        {
            return s1.Equals(s2, StringComparison.OrdinalIgnoreCase);
        }

        public static bool StringContainsIgnoreCase(string source, string search)
        {
            return source.IndexOf(search, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        public static Range ParseRangeHeader(string rangeString)
        {
            if(Regex.IsMatch(rangeString, "(range=)(\\d+)(-)(\\d+)"))
            {
                throw new ArgumentException();
            }

            rangeString = rangeString.Replace("range=", "");
            string[] split = rangeString.Split('-');
            var range = new Range
            {
                Lower = int.Parse(split[0]),
                Upper = int.Parse(split[1])
            };
            return range;
        }
    }
}