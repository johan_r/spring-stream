﻿using Server.DB;
using Server.Model;
using Server.Model.Database;
using Server.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Server.Controller
{
    [EnableCors(origins: "http://localhost:53720", headers: "*", methods: "*")]
    [RoutePrefix("api/v1/songs")]
    public class SongsController : ApiController
    {
        [Route("clear")]
        [HttpPost]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "Admin")]
        public void Clear()
        {
            // Clear current records of songs, artists and playlists
            DatabaseAccessor.Songs.Clear();
        }

        [Route("update")]
        [HttpPost]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "Admin")]
        public void Update()
        {
            // Get metadata from songs on disk
            List<SongMetadata> metaList = IO.GetSongsMetadata();

            // Get songs currently in db in order to skip songs already present
            List<Song> songsInDb = DatabaseAccessor.Songs.GetAllSongs();
            
            var artists = new List<Artist>();
            var songs = new List<Song>();
            var artistSongs = new List<ArtistSong>();
            foreach (var meta in metaList)
            {
                // Check if the song already exists in the db, if so don't add it again
                var songInDb = songsInDb.FirstOrDefault(s => s.Md5Checksum.SequenceEqual(meta.Md5Checksum));
                if(songInDb != null)
                {
                    Debug.WriteLine("Song already in database: " + meta.Title); // TODO: Log

                    // If the file has been moved, update its path
                    if(songInDb.Path != meta.Path)
                    {
                        var result = DatabaseAccessor.Songs.UpdateSongPath(meta.Md5Checksum, meta.Path);
                        Debug.WriteLine("Path changed from " + songInDb.Path + " to " + meta.Path + ". Update path success: " + result); // TODO: Log
                    }
                    continue;
                }

                // Artists
                foreach (var metaArtist in meta.Artists)
                {
                    var existingArtist = artists.FirstOrDefault(
                        a => Util.StringEqualsIgnoreCase(a.ArtistName, metaArtist.ArtistName));

                    if (existingArtist == null)
                    {
                        var artist = new Artist
                        {
                            ArtistID = RT.Comb.Provider.Sql.Create(),
                            ArtistName = metaArtist.ArtistName
                        };
                        artists.Add(artist);
                        metaArtist.ArtistID = artist.ArtistID;
                    }
                    else
                    {
                        metaArtist.ArtistID = existingArtist.ArtistID;
                    }
                }

                // Songs
                var song = new Song
                {
                    SongID = RT.Comb.Provider.Sql.Create(),
                    Path = meta.Path,
                    Title = meta.Title,
                    DateAdded = DateTime.Now,
                    Duration = meta.Duration,
                    InvariantStartPosition = meta.InvariantStartPosition,
                    InvariantEndPosition = meta.InvariantEndPosition,
                    Md5Checksum = meta.Md5Checksum
                };
                songs.Add(song);
                meta.SongID = song.SongID;

                // Artist-song relations
                foreach (var metaArtist in meta.Artists)
                {
                    var existingArtistSong = artistSongs.FirstOrDefault(
                       a => a.ArtistID == metaArtist.ArtistID && a.SongID == meta.SongID);

                    if (existingArtistSong == null)
                    {
                        var artistSong = new ArtistSong
                        {
                            ArtistID = metaArtist.ArtistID,
                            SongID = meta.SongID
                        };
                        artistSongs.Add(artistSong);
                    }
                }
            }
            DatabaseAccessor.Songs.AddArtists(artists);
            DatabaseAccessor.Songs.AddSongs(songs);
            DatabaseAccessor.Songs.AddArtistSongs(artistSongs);
        }

        [Route("all-songs")]
        [HttpGet]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "User,Admin")]
        public List<Song> GetAllSongs()
        {
            var songs = DatabaseAccessor.Songs.GetAllSongs();
            PrepareSongsForClient(songs);
            return songs;
        }

        [Route("playlist-songs/{playlistId}")]
        [HttpGet]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "User,Admin")]
        public List<Song> GetPlaylistSongs(Guid playlistId)
        {
            var songs = DatabaseAccessor.Songs.GetPlaylistSongs(playlistId);
            PrepareSongsForClient(songs);
            return songs;
        }

        [Route("song/{id}")]
        [HttpGet]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "User,Admin")]
        public HttpResponseMessage GetSong(Guid id) // TODO CANCELLATION TOKEN
        {
            if(Request.Headers.Range == null)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
            }

            var range = Request.Headers.Range.Ranges.First();
            if (range == null || range.From == null || range.To == null)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
            }

            // If requesting start of a song - add a songPlay statistic to database
            Request.Headers.TryGetValues("IsFirstChunk", out IEnumerable<string> isFirstChunkHeader);
            var isFirstChunk = bool.Parse(isFirstChunkHeader.First());
            if (isFirstChunk)
            {
                int userId = Jwt.GetUserIdFromToken(Request.Headers.Authorization.Parameter);
                DatabaseAccessor.Statistics.AddSongPlay(id, userId);
            }

            try
            {
                var songPath = DatabaseAccessor.Songs.GetSongPath(id);
                byte[] song = IO.GetSongChunk(songPath, (long)range.From, (long)range.To);
                HttpResponseMessage response = Request.CreateResponse();
                var content = new ByteArrayContent(song);
                response.Content = content;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("audio/mpeg");
                return response;
            }
            catch(Exception)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
            }
        }

        private void PrepareSongsForClient(List<Song> songs)
        {
            foreach (var song in songs)
            {
                song.Path = null;
                song.Md5Checksum = null;
            }
        }
    }
}