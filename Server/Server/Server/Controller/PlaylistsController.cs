﻿using Server.DB;
using Server.Model.Database;
using Server.Model.HttpRequest;
using Server.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Server.Controller
{
    [EnableCors(origins: "http://localhost:53720", headers: "*", methods: "*")]
    [RoutePrefix("api/v1/playlists")]
    public class PlaylistsController : ApiController
    {
        [Route("get-user-playlists")]
        [HttpGet]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "User,Admin")]
        public List<Playlist> GetUserPlaylists()
        {
            int userId = Jwt.GetUserIdFromToken(Request.Headers.Authorization.Parameter);
            return DatabaseAccessor.Playlists.GetUserPlaylists(userId);
        }

        [Route("create")]
        [HttpPost]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "User,Admin")]
        public Guid CreatePlaylist([FromBody]string playlistName)
        {
            if(string.IsNullOrWhiteSpace(playlistName))
            {
                return Guid.Empty;
            }

            int userId = Jwt.GetUserIdFromToken(Request.Headers.Authorization.Parameter);
            var playlist = new Playlist
            {
                PlaylistId = RT.Comb.Provider.Sql.Create(),
                UserId = userId,
                Name = playlistName
            };

            DatabaseAccessor.Playlists.CreatePlaylist(playlist);
            return playlist.PlaylistId;
        }

        [Route("delete")]
        [HttpPost]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "User,Admin")]
        public bool DeletePlaylist([FromBody]Guid playlistId)
        {
            if (playlistId == null)
            {
                return false;
            }

            DatabaseAccessor.Playlists.DeletePlaylist(playlistId);
            return true;
        }

        [Route("add-song-to-playlist")]
        [HttpPost]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "User,Admin")]
        public bool AddSongToPlaylist(PlaylistSongRequest request)
        {
            if (request == null || request.PlaylistId == null || request.SongId == null)
            {
                return false;
            }

            return DatabaseAccessor.Playlists.AddSongToPlaylist(request.PlaylistId, request.SongId);
        }

        [Route("remove-song-from-playlist")]
        [HttpPost]
        [JwtAuthenticationFilter]
        [Authorize(Roles = "User,Admin")]
        public bool RemoveSongFromPlaylist(PlaylistSongRequest request)
        {
            if (request == null || request.PlaylistId == null || request.SongId == null)
            {
                return false;
            }

            DatabaseAccessor.Playlists.RemoveSongFromPlaylist(request.PlaylistId, request.SongId);
            return true;
        }
    }
}