﻿using Server.DB;
using Server.Model;
using Server.Model.Database;
using Server.Model.HttpRequest;
using Server.Model.HttpResponse;
using Server.Security;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Server.Controller
{
    [EnableCors(origins: "http://localhost:53720", headers: "*", methods: "*")]
    [RoutePrefix("api/v1/users")]
    public class UsersController : ApiController
    {
        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public UserLoginResponse PostLogin(UserLoginRequest request)
        {
            if (request == null
                || string.IsNullOrEmpty(request.Username)
                || string.IsNullOrEmpty(request.Password))
            {
                return new UserLoginResponse { Success = false };
            }

            User user = DatabaseAccessor.Users.GetUserByUsername(request.Username); // TODO: Make this + email checks case insensitive (ALSO ON REGISTER)
            if (user == null)
            {
                return new UserLoginResponse { Success = false };
            }

            bool success = Crypto.AuthenticateUser(request.Password, user.Password, user.PasswordSalt, user.PasswordIterations);
            if (success)
            {
                string[] userRoles = DatabaseAccessor.Users.GetUserRoles(user.UserID);
                string authToken = Jwt.GenerateJwtToken(user.UserID, user.Username, userRoles);
                return new UserLoginResponse { Success = true, AuthToken = authToken };
            }
            else
            {
                return new UserLoginResponse { Success = false };
            }
        }

        [Route("register")]
        [HttpPost]
        [JwtAuthenticationFilter]
        [Authorize(Roles ="Admin")]
        public string PostRegister(UserRegisterRequest request)
        {
            // Assert the user may be registered
            if(request == null 
                || string.IsNullOrEmpty(request.Username)
                || string.IsNullOrEmpty(request.Email) 
                || string.IsNullOrEmpty(request.Password))
            {
                return "Registration failed: Request data missing";
            }

            if (DatabaseAccessor.Users.UsernameExists(request.Username))
            {
                return "Registration failed: Username already exists";
            }
            if (DatabaseAccessor.Users.EmailExists(request.Email))
            {
                return "Registration failed: Email address already exists";
            }

            // Hash password
            PasswordHash hash = Crypto.GeneratePasswordHash(request.Password);

            var registerDate = DateTime.Now;

            // Register the user
            DatabaseAccessor.Users.AddUser(request.Username, request.Email, hash.Password, hash.Salt, hash.Iterations, registerDate);
            return "Registration successful";
        }
    }
}